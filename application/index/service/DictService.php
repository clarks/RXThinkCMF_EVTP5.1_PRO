<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\service;

use app\index\model\Dict;

/**
 * 字典管理-服务类
 * @author 牧羊人
 * @since 2020/11/20
 * Class DictService
 * @package app\index\service
 */
class DictService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/20
     * DictService constructor.
     */
    public function __construct()
    {
        $this->model = new Dict();
    }

    /**
     * 获取字典列表
     * @return array
     * @since 2020/11/20
     * @author 牧羊人
     */
    public function getList()
    {
        $param = request()->param();
        // 查询条件
        $map = [];
        // 字典类型ID
        $dicttypeId = getter($param, "dicttypeId", 0);
        if ($dicttypeId) {
            $map[] = ['dicttype_id', '=', $dicttypeId];
        }
        // 字典名称
        $name = getter($param, "name");
        if ($name) {
            $map[] = ['name', 'like', "%{$name}%"];
        }
        // 字典编码
        $code = getter($param, 'code');
        if ($code) {
            $map[] = ['code', '=', $code];
        }
        return parent::getList($map); // TODO: Change the autogenerated stub
    }

}