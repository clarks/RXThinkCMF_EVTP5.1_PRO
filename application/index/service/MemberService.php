<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\service;

use app\index\model\Member;

/**
 * 会员管理-服务类
 * @author 牧羊人
 * @since 2020/11/20
 * Class MemberService
 * @package app\index\service
 */
class MemberService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/20
     * MemberService constructor.
     */
    public function __construct()
    {
        $this->model = new Member();
    }

    /**
     * 添加或编辑
     * @return mixed
     * @since 2020/11/20
     * @author 牧羊人
     */
    public function edit()
    {
        // 请求参数
        $data = request()->param();
        // 头像处理
        $avatar = getter($data, 'avatar');
        if (strpos($avatar, "temp")) {
            $data['avatar'] = save_image($avatar, 'member');
        } else {
            $data['avatar'] = str_replace(IMG_URL, "", $data['avatar']);
        }

        // 城市数据处理
        $city = $data['city'];
        if (!empty($city)) {
            $data['province_id'] = $city[0];
            $data['city'] = $city[1];
            $data['district_id'] = $city[2];
        } else {
            $data['province_id'] = 0;
            $data['city'] = 0;
            $data['district_id'] = 0;
        }
        return parent::edit($data); // TODO: Change the autogenerated stub
    }

}