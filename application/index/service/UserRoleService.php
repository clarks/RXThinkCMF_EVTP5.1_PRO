<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\service;

use app\index\model\UserRole;

/**
 * 用户角色-服务类
 * @author 牧羊人
 * @since 2020/11/19
 * Class UserRoleService
 * @package app\index\service
 */
class UserRoleService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/19
     * UserRoleService constructor.
     */
    public function __construct()
    {
        $this->model = new UserRole();
    }

    /**
     * 获取用户角色列表
     * @param $userId 用户ID
     * @return mixed
     * @author 牧羊人
     * @since 2020/11/19
     */
    public function getUserRoleList($userId)
    {
        $roleList = $this->model->alias("ur")
            ->field('r.*')
            ->join(DB_PREFIX . 'role r', 'ur.role_id=r.id')
            ->distinct(true)
            ->where('ur.user_id', '=', $userId)
            ->where('r.status', '=', 1)
            ->where('r.mark', '=', 1)
            ->order('r.sort asc')
            ->select()->toArray();
        return $roleList;
    }

    /**
     * 删除用户角色
     * @param $userId 用户ID
     * @throws \Exception
     * @author 牧羊人
     * @since 2020/11/19
     */
    public function deleteUserRole($userId)
    {
        $this->model->where("user_id", '=', $userId)->delete();
    }

    /**
     * 批量插入用户角色关系
     * @param $userId 用户ID
     * @param $roleIds 角色ID集合
     * @author 牧羊人
     * @since 2020/11/19
     */
    public function insertUserRole($userId, $roleIds)
    {
        if (!empty($roleIds)) {
            $list = [];
            foreach ($roleIds as $val) {
                $data = [
                    'user_id' => $userId,
                    'role_id' => $val,
                ];
                $list[] = $data;
            }
            $this->model->insertAll($list);
        }
    }

}