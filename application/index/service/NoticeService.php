<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\service;

use app\index\model\Notice;

/**
 * 通知公告-服务类
 * @author 牧羊人
 * @since 2020/11/20
 * Class NoticeService
 * @package app\index\service
 */
class NoticeService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/20
     * NoticeService constructor.
     */
    public function __construct()
    {
        $this->model = new Notice();
    }

    /**
     * 设置置顶
     * @return array
     * @throws \think\db\exception\BindParamException
     * @throws \think\exception\PDOException
     * @since 2020/11/21
     * @author 牧羊人
     */
    public function setIsTop()
    {
        $data = request()->param();
        if (!$data['id']) {
            return message('记录ID不能为空', false);
        }
        if (!$data['is_top']) {
            return message('是否置顶不能为空', false);
        }
        $error = '';
        $item = [
            'id' => $data['id'],
            'is_top' => $data['is_top']
        ];
        $rowId = $this->model->edit($item, $error);
        if (!$rowId) {
            return message($error, false);
        }
        return message();
    }

}