<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\behavior;

use think\Env;

class InitApp
{

    /**
     * 执行行为 run方法是Behavior唯一的接口
     * @author 牧羊人
     * @since 2020/11/19
     */
    public function run()
    {
        // 初始化系统常量
        $this->initSystemConst();

        // 初始化数据库
        $this->initDbInfo();

    }

    /**
     * 初始化系统常量
     *
     * @author 牧羊人
     * @date 2019-04-24
     */
    private function initSystemConst()
    {
        // 基础常量
        define('THINK_PATH', \Env::get('think_path'));
        define('ROOT_PATH', \Env::get('root_path'));
        define('APP_PATH', \Env::get('app_path'));
        define('CONFIG_PATH', \Env::get('config_path'));
        define('ROUTE_PATH', \Env::get('route_path'));
        define('RUNTIME_PATH', \Env::get('runtime_path'));
        define('EXTEND_PATH', \Env::get('extend_path'));
        define('VENDOR_PATH', \Env::get('vendor_path'));
        define('PLUGIN_PATH', ROOT_PATH . 'plugins');
        define('PUBLIC_PATH', ROOT_PATH . 'public');

        // 附件常量
        define('ATTACHMENT_PATH', PUBLIC_PATH . "/uploads");
        define('IMG_PATH', ATTACHMENT_PATH . "/images");
        define('UPLOAD_TEMP_PATH', ATTACHMENT_PATH . '/temp');
        define('IMG_URL', \Env::get('domain.image_url'));
    }

    /**
     * 初始化数据库
     * @author 牧羊人
     * @date 2019/4/23
     */
    private function initDbInfo()
    {
        // 数据表前缀
        define('DB_PREFIX', \Env::get('database.prefix'));
    }

}