<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\behavior;

use think\Response;

/**
 * 跨域解决方案
 * @author 牧羊人
 * @since 2020/11/19
 * Class CORS
 * @package app\index\behavior
 */
class CORS
{

    /**
     * 执行行为 run方法是Behavior唯一的接口
     * @author 牧羊人
     * @since 2020/11/19
     */
    public function run()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Authorization,token,Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: POST,GET');
        if (request()->isOptions()) {
            exit();
        }
    }

}