<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\behavior;


use app\index\model\ActionLog;

class UserLog
{
    /**
     * 执行行为 run方法是Behavior唯一的接口
     * @param $params 参数
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author 牧羊人
     * @since 2020/11/21
     */
    public function run()
    {
        if (IS_POST) {
            // 记录行为日志(登录后记录)
            ActionLog::record();
        }

    }
}