<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\controller;

use app\index\service\MenuService;
use app\index\service\UserService;

/**
 * 后台主页-控制器
 * @author 牧羊人
 * @since 2020/11/19
 * Class Index
 * @package app\index\controller
 */
class Index extends Backend
{

    /**
     * 获取菜单列表
     * @return mixed
     * @since 2020/11/19
     * @author 牧羊人
     */
    public function getMenuList()
    {
        $menuService = new MenuService();
        $result = $menuService->getPermissionList($this->userId);
        return $this->jsonReturn($result);
    }

    /**
     * 获取用户信息
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @since 2020/11/19
     * @author 牧羊人
     */
    public function getUserInfo()
    {
        $userService = new UserService();
        $result = $userService->getUserInfo($this->userId);
        return $this->jsonReturn($result);
    }

    /**
     * 更新个人资料
     * @return mixed
     * @since 2020/11/19
     * @author 牧羊人
     */
    public function updateUserInfo()
    {
        $userService = new UserService();
        $result = $userService->updateUserInfo();
        return $this->jsonReturn($result);
    }

    /**
     * 更新密码
     * @return mixed
     * @throws \think\db\exception\BindParamException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @author 牧羊人
     * @since 2020/11/19
     */
    public function updatePwd()
    {
        $userService = new UserService();
        $result = $userService->updatePwd($this->userId);
        return $this->jsonReturn($result);
    }
}