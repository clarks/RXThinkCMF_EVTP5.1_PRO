<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\controller;

use app\index\service\AdSortService;

/**
 * 广告位管理-控制器
 * @author 牧羊人
 * @since 2020/11/20
 * Class Adsort
 * @package app\index\controller
 */
class Adsort extends Backend
{
    /**
     * 初始化
     * @author 牧羊人
     * @since 2020/11/20
     */
    protected function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->service = new AdSortService();
    }

    /**
     * 获取广告位列表
     * @return mixed
     * @since 2020/11/20
     * @author 牧羊人
     */
    public function getAdSortList()
    {
        $result = $this->service->getAdSortList();
        return $this->jsonReturn($result);
    }

}