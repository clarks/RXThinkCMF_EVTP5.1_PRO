<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\index\model;


/**
 * 用户角色-模型
 * @author 牧羊人
 * @since 2020/11/19
 * Class UserRole
 * @package app\index\model
 */
class UserRole extends BaseModel
{
    // 设置数据表名
    protected $name = 'user_role';
}