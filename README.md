
<h1 align="center"> 为梦想而创作：RXThinkCMF_EVTP5.1_PRO 前后端分离框架 </h1>

<p align="center">
	<a href="http://www.rxthink.cn/">
	    <img src="https://img.shields.io/hexpm/l/plug.svg" />
	</a>
	<a href='https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP5.1_PRO/stargazers'>
	    <img src='https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP5.1_PRO/badge/star.svg?theme=gray' alt='star'></img>
	</a>
	<a href='https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP5.1_PRO/members'>
	    <img src='https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP5.1_PRO/badge/fork.svg?theme=gray' alt='fork'></img>
	</a>
</p>

## 项目介绍
RXThinkCMF_EVTP5.1_PRO 旗舰版是一款基于 ThinkPhp5.1 + Vue + ElementUI研发的前后端分离框架，随着系统框架的精细化发展，越来越多的项目采用前后端分离的架构，因此我们重点打造了一款前后端分离架构的PHP框架，为了简化开发，框架集成了完整的权限架构体系以及常规基础模块如：用户管理、角色管理、菜单管理、职级管理、岗位管理、日志管理以及广告管理、页面布局管理、字典管理、配置管理等等，前端UI支持多主题切换，可以设置自己喜欢的样式风格，同时框架集成了代码生成器可以一键生成CURD所有模块文件及代码(同时会自动生成前端UI代码)，以便开发者快速构建自己的应用。专注于为中小企业提供最佳的行业基础后台框架解决方案，提高执行效率、扩展性、稳定性值得信赖，操作体验流畅，欢迎大家使用！！
 

## 环境要求:
* PHP >= 7.1
* PDO PHP Extension
* MBstring PHP Extension
* CURL PHP Extension
* 开启静态重写
* 要求环境支持pathinfo
* 要求安装Zip扩展(插件/模块市场需要)


### 功能特性
- **严谨规范：** 提供一套有利于团队协作的结构设计、编码、数据等规范。
- **高效灵活：** 清晰的分层设计、钩子行为扩展机制，解耦设计更能灵活应对需求变更。
- **严谨安全：** 清晰的系统执行流程，严谨的异常检测和安全机制，详细的日志统计，为系统保驾护航。
- **组件化：** 完善的组件化设计，丰富的表单组件，让开发列表和表单更得心应手。无需前端开发，省时省力。
- **简单上手快：** 结构清晰、代码规范、在开发快速的同时还兼顾性能的极致追求。
- **自身特色：** 权限管理、组件丰富、第三方应用多、分层解耦化设计和先进的设计思想。
- **高级进阶：** 分布式、负载均衡、集群、Redis、分库分表。 
- **命令行：** 命令行功能，一键管理应用扩展。 


## 开发者信息
* 系统名称：RXThinkCMF_EVTP5.1_PRO前后端分离框架  
* 作者[牧羊人]：南京RXThinkCMF研发中心
* 作者QQ：1175401194  
* 官网网址：[http://www.rxthink.cn/](http://www.rxthink.cn/)  
* 文档网址：[http://docs.evtp5.1.pro.rxthink.cn/](http://docs.evtp5.1.pro.rxthink.cn/)  
* 开源协议：Apache 2.0

### RXThinkCMF版本说明

| 版本名称 | 说明 | 地址 |
| :---: | :---: | :---: |
| RXThinkCMF_TP5.1专业版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP5.1 |
| RXThinkCMF_TP5.1旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP5.1_PRO |
| RXThinkCMF_EVTP5.1_PRO旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP5.1_PRO |


## 在线演示

- 演示地址：[http://manage.evtp5.1.pro.rxthink.cn/](http://manage.evtp5.1.pro.rxthink.cn/)
- 登录账号：admin
- 登录密码：123456

## 技术支持

[技术支持QQ：1175401194](http://wpa.qq.com/msgrd?v=3&amp;uin=1175401194&amp;site=qq&amp;menu=yes)


## 效果展示

#### 1、系统登录
 ![系统登录](http://images.evtp5.1.pro.rxthink.cn/demo/1.png)
 
#### 2、工作台
 ![工作台](http://images.evtp5.1.pro.rxthink.cn/demo/2.png)
 
#### 3、数据分析
 ![数据分析](http://images.evtp5.1.pro.rxthink.cn/demo/3.png)
 
#### 4、大屏统计
 ![大屏统计](http://images.evtp5.1.pro.rxthink.cn/demo/4.png)
 
#### 5、用户管理
 ![用户管理](http://images.evtp5.1.pro.rxthink.cn/demo/5.png)
 
#### 6、角色管理
 ![角色管理](http://images.evtp5.1.pro.rxthink.cn/demo/6.png)
 
#### 7、菜单管理
 ![菜单管理](http://images.evtp5.1.pro.rxthink.cn/demo/7.png)
 
#### 8、职级管理
 ![职级管理](http://images.evtp5.1.pro.rxthink.cn/demo/8.png)
 
#### 9、岗位管理
![岗位管理](http://images.evtp5.1.pro.rxthink.cn/demo/9.png)

#### 10、部门管理
![部门管理](http://images.evtp5.1.pro.rxthink.cn/demo/10.png)

#### 11、字典管理
![字典管理](http://images.evtp5.1.pro.rxthink.cn/demo/11.png)

#### 12、城市管理
![城市管理](http://images.evtp5.1.pro.rxthink.cn/demo/12.png)

#### 13、配置管理
![配置管理](http://images.evtp5.1.pro.rxthink.cn/demo/13.png)

#### 14、通知公告
![通知公告](http://images.evtp5.1.pro.rxthink.cn/demo/13.png)

#### 15、个人资料
![个人资料](http://images.evtp5.1.pro.rxthink.cn/demo/15.png)

#### 16、友链管理
![友链管理](http://images.evtp5.1.pro.rxthink.cn/demo/16.png)

#### 17、会员管理
![会员管理](http://images.evtp5.1.pro.rxthink.cn/demo/17.png)

#### 18、代码生成器
![文章管理](http://images.evtp5.1.pro.rxthink.cn/demo/18.png)

## 部署说明

[点击查看技术文档](http://docs.evtp5.1.pro.rxthink.cn/)

## 安全&缺陷
如果你发现了一个安全漏洞或缺陷，请发送邮件到 1175401194@qq.com,所有的安全漏洞都将及时得到解决。


## 鸣谢
感谢[ThinkPHP](http://www.thinkphp.cn)、[VueJs](https://cn.vuejs.org/)、[ElementUI](https://element.eleme.cn/#/zh-CN)等优秀开源项目。

## 版权信息

提供个人非商业用途免费使用，商业需授权。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2017-2020 rxthink.cn (http://www.rxthink.cn)

All rights reserved。

更多细节参阅 [LICENSE.txt](LICENSE.txt)